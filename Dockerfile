FROM fluxcd/flux-cli:v2.1.2

ARG TARGETOS
ARG TARGETARCH

# renovate: datasource=github-releases depName=sigstore/cosign VersionTemplate=v
ENV COSIGN_VERSION "2.4.3"

USER root

# Copy requirements file
WORKDIR /
COPY requirements.txt .

# Update package list and install tools
# hadolint ignore=DL3018
RUN apk add --no-cache curl wget bash coreutils py3-yaml py3-jsonschema py3-pip jq yq gcc g++ linux-headers python3-dev && \
    # Install oras CLI (no official package available, using curl)
    curl -Lo oras.tar.gz https://github.com/oras-project/oras/releases/download/v1.1.0/oras_1.1.0_${TARGETOS}_${TARGETARCH}.tar.gz && \
    tar -zxvf oras.tar.gz oras && mv oras /usr/local/bin/ && \
    # Install kubectl
    curl -LO https://dl.k8s.io/release/v1.28.4/bin/${TARGETOS}/${TARGETARCH}/kubectl && \
    chmod +x kubectl && mv kubectl /usr/local/bin && \
    pip install --no-cache-dir -r requirements.txt && \
    adduser --disabled-password --gecos '' --uid 1001 appuser \
    # install cosign
    && curl -LO https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign-${TARGETOS}-${TARGETARCH} \
    && chmod +x cosign-${TARGETOS}-${TARGETARCH} && mv cosign-${TARGETOS}-${TARGETARCH} /usr/local/bin/cosign

# Switch to the non-root user
USER 1001